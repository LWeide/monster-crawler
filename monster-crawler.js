#!/usr/bin/env node

'use strict';
const fetch = require('node-fetch');

const { Pool } = require('pg')
const client = new Pool({
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PW,
    database: process.env.DB,
    port: 5432
});

const min = Number.parseInt(process.env.MIN_CRAWL)
const max = Number.parseInt(process.env.MAX_CRAWL) || max + 10000;

const iterate = (jobid) => {
    if (jobid >= max) {
        console.log("done");
        return;
    }
    const url = "https://job-openings.monster.com/v2/job/pure-json-view?jobid=" + jobid;
    fetch(url).then(response => response.json()).then(({ jobId, companyInfo, salary, jobLocationCountry, jobLocationRegion, jobLocationCity, jobDescription, jobCategory, jobIndustry, postedDate }) => {
        var v = {
            "jobid": jobId,
            "datasource": "monster.com",
            "companyname": companyInfo.name,
            "companyheader": companyInfo.companyHeader,
            "companyindustry": companyInfo.industryName,
            "companysize": companyInfo.companySizeName,
            "companyhq": companyInfo.hqLocation,
            "companydesc": companyInfo.description,
            "companysumm": null,
            "salaryrange": salary ? salary.rangeText : null,
            "salaryestimate": salary ? salary.estimation : null,
            "joblocationcountry": jobLocationCountry,
            "joblocationregion": jobLocationRegion,
            "joblocationcity": jobLocationCity,
            "jobdescription": jobDescription,
            "jobcategory": jobCategory,
            "jobindustry": jobIndustry,
            "posteddate": postedDate,
        };
        console.log(`Job ID: ${v.jobid}\nJob Offer: ${v.companyheader}\nJob Category: ${v.jobcategory}\npostedDate: ${v.posteddate}`);
        console.log("test 1")
        var keys = Object.keys(v);
        const query = "INSERT INTO data (" + keys.join(" ,") + ") VALUES(" + keys.map((_, i) => "$" + (i + 1)).join(" ,") + ")";
        if(v.companyheader !== undefined && !checkExistingEntry(v.jobDescription)) {
            console.log("test 2")
            client.query(query, keys.map(key => v[key]), (err) => console.log(err ? err.stack : "saved to db"));
        }
    }).catch(ex => console.error(ex));
    setTimeout(() => iterate(jobid + 1), 2000);
}
iterate(min);

function checkExistingEntry(description){
    console.log("test 3")
    const query = `SELECT * FROM data WHERE jobdescription = '${description}'`;
    client.query(query, (err, result) => {
        console.log("Result " + result);
        if (err) {
            result.json({ success: false, error: err.toString() });
            
        } else {
            if(result != null) {
                
                console.log("test 4")
                return true;
            }
            console.log("test 5")
            return false;
        }
        
    });
}