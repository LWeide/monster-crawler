#!/usr/bin/env node

'use strict';
var fs = require('fs');

const filter = (w)=>{
    return w.length>3 && ! ([''].some(d=>w==d));
}

const concat = (x,y) =>
  x.concat(y)

const flatMap = (f,xs) =>
  xs.map(f).reduce(concat, [])

Array.prototype.flatMap = function(f) {
  return flatMap(f,this)
}

fs.readFile(__dirname + '../samples/samplebody2.html', function (err, data) {
    if (err) {
        throw err;
    }

    const matches = ["job skills", "success factor", "qualifications", "certification"];

    /*
    x.replace(/<[^>]*>?/gm, '').replace(/[^a-z ]/g, "").replace(/( )+/g, " ")
    */

    const str = data.toString().toLowerCase();
    var col = str.split(/<li>|<p>|<br>/g);
    col = col.map(x=>x.replace(/<[^>]*>?/gm, '').replace(/\n/gm, '').trim()).filter(x=>x!='&nbsp;');
    col = col.map(x=>x.split(" ").map(y=>y.trim()).filter(y=>y!="").join(" "));
    console.log(col);

    /*
    const str = data.toString().toLowerCase().replace(/<[^>]*>?/gm, '').replace(/[^a-z ]/g, "").split(" ").filter(x=>filter(x)).reduce(function (prev, item) {
        if (item in prev) prev[item]++;
        else prev[item] = 1;
        return prev;
    }, {});
    console.log(str);
    */

});