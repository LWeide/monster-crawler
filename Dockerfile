FROM node:latest

LABEL maintainer="dominik.brosch@betafase.com"

ADD monster-crawler.js /app/monster-crawler.js
ADD package.json /app/package.json
ADD yarn.lock /app/yarn.lock
WORKDIR /app
RUN yarn install

ENTRYPOINT ["yarn", "start"]