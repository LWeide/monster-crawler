

## Run Crawler
This will run the crawler on any docker host. Parameters:
MIN_CRAWL=jobid to start crawling (inclusive)
MAX_CRAWL=jobid to stop crawling (exclusive)
APITOKEN=token for uploading data to sql server

The crawler calls a page every 2 seconds, so you can approximate the run duration from that. The approach has been tested for up to 25K offers at a time.

### Run on any docker host

```bash
docker run --rm -it -e MIN_CRAWL=217000000 -e MAX_CRAWL=217010000 -E APITOKEN=2023 dkbrosch/seminar-agile-crawler:latest
```


### Build locally
```bash
docker build -t seminar/crawler:latest .
```

## Fields that should be extracted
-> See example_raw.json

```javascript
companyinfo.name // name of the company
companyinfo.companyheader //equal to job title
companyinfo.companysizename
companyinfo.industryname
companyinfo.hqlocation
companyinfo.description
conpanyinfo.profileurl
summary.info // object

salary.rangetext // salary estimation, if available
salary.estimation
joblocationcountry
joblocationregion
joblocationcity
jobdescription
jobid
jobcategory
jobocccategory // is array
positiontype // is array
jobindustry // is array

```
